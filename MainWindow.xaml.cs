﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using Dragablz;
using PromptDialog;
using System.Collections.ObjectModel;
using System.Data;
using ICSharpCode.AvalonEdit;

namespace RimWorld_Def_Editor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private FolderBrowserDialog folderBrowserDialog;
        private TabManager tabManager;
        public static XMLValidator Validator;
        public static XMLTextChangeManager TextChangeManager;
        public SourceFolderHandler SourceHandler;
        public static MenuItem DefTypeMenuItem;
#warning DEBUG ACTIVE
        private bool DEBUG = true;

        public MainWindow()
        {
            InitializeComponent();
            folderBrowserDialog = new FolderBrowserDialog();
            Validator = new XMLValidator(btnValidationTimer, txtCurrentIssuesInfo, txtColorizedIssues);
            SourceHandler = new SourceFolderHandler(panelFolderSources);
            gridModSources.DataContext = SourceHandler.ModEntries.DefaultView;
            tabManager = new TabManager(tabXMLInputFiles);
            TextChangeManager = new XMLTextChangeManager(comboBoxActiveDefType);
            DefTypeMenuItem = MenuItemDefTypes;

            if (DEBUG)
            {
                SourceHandler.BaseGame.Path = "C:\\Program Files (x86)\\Steam\\steamapps\\common\\RimWorld";
                SourceHandler.BaseGame.TryProcessPath();
                SourceHandler.BaseGame.txtDirectoryPath.Text = "C:\\Program Files (x86)\\Steam\\steamapps\\common\\RimWorld";
                btnRebuildModList_Click(null, null);
            }
        }

        public void OnFileNodeMouseDoubleClick(object sender, EventArgs e)
        {
            if (!(sender is TreeViewItem item))
                return;
            tabManager.CreateFileTab(item);
        }

        private void btnRebuildModList_Click(object sender, MouseButtonEventArgs e)
        {
            SourceHandler.ReloadModEntries();
        }

        private void btnBuildDef_Click(object sender, MouseButtonEventArgs e)
        {

        }

        private void txtXMLEditor_GotFocus(object sender, RoutedEventArgs e)
        {
            if (!(sender is TextEditor editor))
                return;
            TextChangeManager.SetActiveEditor(editor);
        }

        private void btnToggleModSourcesVisibility_Click(object sender, MouseButtonEventArgs e)
        {
            if(gridModSources.Visibility == Visibility.Visible)
            {
                gridModSources.Visibility = Visibility.Collapsed;
                btnToggleModSourcesVisibility.Content = "Show Mods";
            }
            else
            {
                gridModSources.Visibility = Visibility.Visible;
                btnToggleModSourcesVisibility.Content = "Hide Mods";
            }
        }

        private void comboBoxActiveDefType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0)
                return;
            Type? selectedType = e.AddedItems[0] as Type;
            if (selectedType == null)
                return;
            TextChangeManager.SetActiveDefType(selectedType);
        }
    }
}
