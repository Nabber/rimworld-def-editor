﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using MessageBox = System.Windows.Forms.MessageBox;
using Button = System.Windows.Controls.Button;
using TextBox = System.Windows.Controls.TextBox;
using Label = System.Windows.Controls.Label;
using System.Xml;
using System.Reflection;

namespace RimWorld_Def_Editor
{
    public class SourceFolderHandler
    {
        public static SourceFolderHandler Instance { get; private set; }
        public DataTable ModEntries;
        public List<string> LoadedPackageIds;
        public SourceHandler_BaseGame BaseGame;
        public SourceHandler_LocalMods LocalMods;
        public SourceHandler_WorkshopMods WorkshopMods;
        public SourceHandler_Config Config;

        public SourceFolderHandler(StackPanel panel)
        {
            Instance = this;
            BuildTables();
            BaseGame = new SourceHandler_BaseGame();
            LocalMods = new SourceHandler_LocalMods();
            WorkshopMods = new SourceHandler_WorkshopMods();
            Config = new SourceHandler_Config();
            LoadedPackageIds = new List<string>();
            panel.Children.Add(BaseGame.Panel);
            panel.Children.Add(LocalMods.Panel);
            panel.Children.Add(WorkshopMods.Panel);
            panel.Children.Add(Config.Panel);
        }

        public void ReloadModEntries()
        {
            LoadedPackageIds = RetrieveLoadedPackageIds().ToList();

            ModEntries.Rows.Clear();
            XMLTextChangeManager.cachedCompletionData.Clear();
            List<DataRow> mods = new List<DataRow>();
            mods.AddRange(BaseGame.LoadAllMods());
            mods.AddRange(LocalMods.LoadAllMods());
            mods.AddRange(WorkshopMods.LoadAllMods());
            foreach(DataRow mod in mods)
            {
                ModEntries.Rows.Add(mod);
            }

            // blocking point, we need to retrieve the actively loaded assemblies from the mod entry, we can only do that if we access the LoadedFolders
            // which will have taken care of conditionally loaded assemblies
#warning critical blocker for project 
        }

        public void LoadRimWorldAssembly()
        {
            string assemblyPath = $"{BaseGame.Path}/RimWorldWin64_Data/Managed/Assembly-CSharp.dll";
            if (!File.Exists(assemblyPath))
            {
                MessageBox.Show($"Could not find RimWorld assembly under path {assemblyPath}");
                return;
            }
            Assembly assembly = Assembly.LoadFrom(assemblyPath);
            if (assembly == null)
                return;
            AssemblyManager.RWAssembly = new RWAssemblyEntry(assembly);
        }

        private IEnumerable<string> RetrieveLoadedPackageIds()
        {
            if (Config.Path == null)
                yield break;
            string modsConfigPath = $"{Config.Path}/Config/ModsConfig.xml";
            if (!File.Exists(modsConfigPath))
                yield break;
            XmlDocument modsConfig = new XmlDocument();
            modsConfig.LoadXml(File.ReadAllText(modsConfigPath));
            XmlNodeList? knownExpansionsNodes = modsConfig.SelectNodes("ModsConfigData/knownExpansions");
            if(knownExpansionsNodes?[0] != null)
            {
                foreach (XmlNode node in knownExpansionsNodes[0].ChildNodes)
                    yield return node.InnerText;
            }
            XmlNodeList? activeModsNodes = modsConfig.SelectNodes("ModsConfigData/activeMods");
            if (activeModsNodes?[0] != null)
            {
                foreach (XmlNode node in activeModsNodes[0].ChildNodes)
                    yield return node.InnerText;
            }
        }

        private void BuildTables()
        {
            ModEntries = new DataTable();
            ModEntries.Columns.Add(
                new DataColumn()
                {
                    DataType = typeof(int),
                    ColumnName = "ID",
                    ReadOnly = true,
                    Unique = true,
                    AutoIncrement = true,
                }
            );
            ModEntries.Columns.Add(
                new DataColumn()
                {
                    DataType = typeof(string),
                    ColumnName = "Name",
                    ReadOnly = true,
                }
            );
            ModEntries.Columns.Add(
                new DataColumn()
                {
                    DataType = typeof(string),
                    ColumnName = "PackageId",
                }
            );
            ModEntries.Columns.Add(
                new DataColumn()
                {
                    DataType = typeof(string),
                    ColumnName = "Path",
                }
            );
            ModEntries.Columns.Add(
                new DataColumn()
                {
                    DataType = typeof(bool),
                    ColumnName = "IsLoaded",
                }
            );
        }
    }

    public abstract class SourceHandler
    {
        public string Path;
        public DockPanel Panel;
        public TextBox txtDirectoryPath;

        public SourceHandler()
        {
            Panel = new DockPanel()
            {
                LastChildFill = true
            };
            Label lblSourceType = new System.Windows.Controls.Label()
            {
                Content = SourceType
            };
            Button btnOpenPickDirectory = new Button()
            {
                VerticalAlignment = VerticalAlignment.Stretch,
                Content = "...",
                Padding = new Thickness(4, 2, 4, 2),
            };
            DockPanel.SetDock(btnOpenPickDirectory, Dock.Left);
            btnOpenPickDirectory.Click += ShowDirectoryPickDialog;
            txtDirectoryPath = new TextBox()
            {
                IsReadOnly = true
            };
            Panel.Children.Add(lblSourceType);
            Panel.Children.Add(btnOpenPickDirectory);
            Panel.Children.Add(txtDirectoryPath);
            // operation may fail in case the default path guess is incorrect
            try
            {
                if (IsSelectedFolderValid(DefaultPath))
                {
                    Path = DefaultPath;
                    txtDirectoryPath.Text = DefaultPath;
                }
            }
            catch(Exception) { }
        }

        public virtual string DefaultPath => "";
        public abstract string FolderBrowserDescription { get; }
        public abstract string SourceType { get; }
        public abstract bool IsSelectedFolderValid(string path);

        public virtual void ShowDirectoryPickDialog(object? sender, EventArgs e)
        {
            MessageBox.Show(FolderBrowserDescription, "Instructions");

            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.Description = FolderBrowserDescription;
            dialog.UseDescriptionForTitle = true;
            DialogResult result = dialog.ShowDialog();
            if (result != DialogResult.OK)
                return;
            string newPath = dialog.SelectedPath;
            if (!IsSelectedFolderValid(newPath))
            {
                MessageBox.Show("The selected folder does not match the required criteria, be sure to follow the instructions!", "Invalid folder");
                return;
            }
            Path = newPath;
            txtDirectoryPath.Text = Path;
        }
    }

    public abstract class ModSourceHandler : SourceHandler
    {
        // TODO Could use the SteamWorks API to retrieve RimWorld installation folder, but I am unsure of including the assembly into the project - it would fail to launch for standalone users
        public override string DefaultPath => "";
        public virtual string DataPathPostfix => "";

        public IEnumerable<DataRow> LoadAllMods()
        {
            if (!IsSelectedFolderValid(Path))
            {
                yield break;
            }
            string dataPath = Path + DataPathPostfix;
            DataRow mod;
            foreach (string directory in Directory.GetDirectories(dataPath))
            {
                mod = SourceFolderHandler.Instance.ModEntries.NewRow();
                mod["Name"] = new DirectoryInfo(directory).Name;
                mod["Path"] = directory;
                PopulateModDataFromXML(mod, directory);
                yield return mod;
            }
            yield break;
        }

        private void PopulateModDataFromXML(DataRow mod, string directory)
        {
            string aboutPath = $"{directory}/About/About.xml";
            if (File.Exists(aboutPath))
            {
                XmlDocument aboutXML = new XmlDocument();
                aboutXML.LoadXml(File.ReadAllText(aboutPath));
                XmlNode? packageIdNode = aboutXML.SelectSingleNode("ModMetaData/packageId");
                if(packageIdNode != null)
                {
                    string id = packageIdNode.InnerText;
                    mod["PackageId"] = id;
                    mod["IsLoaded"] = SourceFolderHandler.Instance.LoadedPackageIds.Contains(id);
                }
                XmlNode? nameNode = aboutXML.SelectSingleNode("ModMetaData/name");
                if (nameNode != null)
                    mod["Name"] = nameNode.InnerText;
            }
        }
    }

    public class SourceHandler_BaseGame : ModSourceHandler
    {
        public override string DefaultPath => "";
        public override string SourceType => "Base Game";
        public override string FolderBrowserDescription => @"Select the ROOT RimWorld folder - it contains the ""Mods"" folder and the ""RimWorldWin64.exe"" file

Steam users: <STEAM>\\steamapps\\common\\RimWorld";
        public override string DataPathPostfix => "\\Data";
        public override bool IsSelectedFolderValid(string path)
        {
            if (path == null)
                return false;
            if (!File.Exists($"{path}\\RimWorldWin64.exe"))
                return false;
            if (!Directory.Exists($"{path}\\Mods"))
                return false;
            return true;
        }

        public void TryProcessPath()
        {
            if (!IsSelectedFolderValid(Path))
                return;

            SourceFolderHandler.Instance.LocalMods.CrossSetPath($"{Path}\\Mods");
            SourceFolderHandler.Instance.LoadRimWorldAssembly();
        }
    }

    public class SourceHandler_LocalMods : ModSourceHandler
    {
        public override string DefaultPath => "";
        public override string SourceType => "Local Mods";
        public override string FolderBrowserDescription => @"Select the RimWorld/Mods folder - it contains a file named ""Place mods here.txt""

It is recommended to just select the BaseGame folder, which sets this folder as well.";

        public override bool IsSelectedFolderValid(string path)
        {
            if (path == null)
                return false;
            if (!File.Exists($"{path}/Place mods here.txt"))
                return false;
            return true;
        }

        public void CrossSetPath(string newPath)
        {
            if (IsSelectedFolderValid(newPath))
            {
                Path = newPath;
                txtDirectoryPath.Text = Path;
            }
        }
    }
    public class SourceHandler_WorkshopMods : ModSourceHandler
    {
        public override string DefaultPath => "";
        public override string SourceType => "Workshop Mods";
        public override string FolderBrowserDescription => @"Select the steam workshop folder - the Workshop ID for RimWorld is 294100

Usually: <STEAM>\steamapps\workshop\content\294100";

        public override bool IsSelectedFolderValid(string path)
        {
            if (path == null)
                return false;
            string folderName = new DirectoryInfo(path).Name;
            if (folderName == null || folderName != "294100")
                return false;
            string? parentFolderName = Directory.GetParent(path)?.Name;
            if (parentFolderName == null || parentFolderName != "content")
                return false;
            return true;
        }
    }
    public class SourceHandler_Config : SourceHandler
    {
        public override string DefaultPath => $@"C:\Users\{Environment.UserName}\AppData\LocalLow\Ludeon Studios\RimWorld by Ludeon Studios";
                
        public override string SourceType => "Config Folder";
        public override string FolderBrowserDescription => @"Select the RimWorld config folder. It is located here: C:\Users\<USER>\AppData\LocalLow\Ludeon Studios\RimWorld by Ludeon Studios";

        public override bool IsSelectedFolderValid(string path)
        {
            if (path == null)
                return false;
            if (!File.Exists($"{path}/Player.log"))
                return false;
            if (!File.Exists($"{path}/Config/ModsConfig.xml"))
                return false;
            return true;
        }
    }

}
