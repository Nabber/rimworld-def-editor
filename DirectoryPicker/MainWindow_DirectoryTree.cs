﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;

namespace RimWorld_Def_Editor
{
    public partial class MainWindow
    {
        private void btnPickDirectory_Click(object sender, RoutedEventArgs e)
        {
            folderBrowserDialog.SelectedPath = txtDirectoryPath.Text;
            DialogResult result = folderBrowserDialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                txtDirectoryPath.Text = folderBrowserDialog.SelectedPath;
                txtDirectoryPath.ToolTip = txtDirectoryPath.Text;
                progDirectoryLoad.Visibility = Visibility.Visible;
            }
            InitLoadDirectory();
        }

        private void InitLoadDirectory()
        {
            progDirectoryLoad.Value = 0;
            treeDirectories.Items.Clear();
            if (txtDirectoryPath.Text != "" && Directory.Exists(txtDirectoryPath.Text))
                LoadDirectory(txtDirectoryPath.Text);
            else
                System.Windows.MessageBox.Show("You must select a directory", "Error");
        }
        public void LoadDirectory(string directoryPath)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(directoryPath);
            progDirectoryLoad.Maximum = Directory.GetFiles(directoryPath, "*.xml", SearchOption.AllDirectories).Length + Directory.GetDirectories(directoryPath, "**", SearchOption.AllDirectories).Length;

            TreeViewItem item = new TreeViewItem()
            {
                Header = dirInfo.Name,
                Tag = dirInfo.FullName
            };
            treeDirectories.Items.Add(item);
            LoadFiles(directoryPath, item);
            LoadSubDirectories(directoryPath, item);
            RemoveEmptyDirectories(item);
            progDirectoryLoad.Visibility = Visibility.Collapsed;
        }
        private void LoadFiles(string dir, TreeViewItem parentItem)
        {
            string[] files = Directory.GetFiles(dir, "*.xml");

            foreach (string file in files)
            {
                FileInfo fileInfo = new FileInfo(file);
                TreeViewItem item = new TreeViewItem()
                {
                    Header = fileInfo.Name,
                    Tag = fileInfo.FullName,
                };
                item.MouseDoubleClick += OnFileNodeMouseDoubleClick;
                parentItem.Items.Add(item);
                UpdateProgress();
            }
        }
        private void LoadSubDirectories(string dir, TreeViewItem parentItem)
        {
            string[] subdirectoryEntries = Directory.GetDirectories(dir);
            foreach (string subdirectory in subdirectoryEntries)
            {
                DirectoryInfo dirInfo = new DirectoryInfo(subdirectory);
                TreeViewItem item = new TreeViewItem()
                {
                    Header = dirInfo.Name,
                    Tag = dirInfo.FullName
                };
                parentItem.Items.Add(item);
                LoadFiles(subdirectory, item);
                LoadSubDirectories(subdirectory, item);
                UpdateProgress();
            }
        }
        private void UpdateProgress()
        {
            if (progDirectoryLoad.Value < progDirectoryLoad.Maximum)
            {
                progDirectoryLoad.Value++;
                System.Windows.Forms.Application.DoEvents();
            }
        }

        private static bool RemoveEmptyDirectories(TreeViewItem item)
        {
            if (item == null)
                return false;
            List<TreeViewItem> nodesToRemove = new List<TreeViewItem>();
            foreach (TreeViewItem node in item.Items)
            {
                if (RemoveEmptyDirectories(node))
                    nodesToRemove.Add(node);
            }
            foreach (TreeViewItem node in nodesToRemove)
            {
                item.Items.Remove(node);
            }
            return ShouldRemoveNode(item);
        }

        private static bool ShouldRemoveNode(TreeViewItem item)
        {
            if (item == null)
                return false;
            if (item.Header is string stringHeader && stringHeader.EndsWith(".xml"))
                return false;
            return item.Items.Count == 0;
        }
    }
}
