﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace RimWorld_Def_Editor
{
    public static class ReflectionUtilities
    {
        public static bool IsUnsavedField(this FieldInfo field)
        {
            if (AssemblyManager.RWAssembly?.UnsavedAttribute == null)
                return false;
            return field.GetCustomAttribute(AssemblyManager.RWAssembly.UnsavedAttribute) != null;
        }
    }
}
