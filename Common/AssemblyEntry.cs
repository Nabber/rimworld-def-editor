﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace RimWorld_Def_Editor
{
    public static class AssemblyManager
    {
        private static RWAssemblyEntry? rwAssembly;
        public static RWAssemblyEntry? RWAssembly
        {
            get => rwAssembly;
            set
            {
                rwAssembly = value;
                RefreshTypesLoadedBy(RWAssembly);
            }
        }
        public static List<AssemblyEntry> ModAssemblies = new List<AssemblyEntry>();
        private static List<Type> allTypes = new List<Type>();

        public static IReadOnlyCollection<Type> AllTypes => allTypes.AsReadOnly();

        public static void AddModAssembly(Assembly assembly)
        {
            AssemblyEntry? matchingEntry = ModAssemblies.Find(entry => entry.Assembly == assembly);
            if (matchingEntry == null)
            {
                matchingEntry = new AssemblyEntry(assembly);
                ModAssemblies.Add(matchingEntry);
            }
            else
            {
                matchingEntry.Update(assembly);
                RefreshTypesLoadedBy(matchingEntry);
            }
        }

        public static void RemoveModAssembly(Assembly assembly)
        {
            AssemblyEntry? matchingEntry = ModAssemblies.Find(entry => entry.Assembly == assembly);
            if (matchingEntry == null)
                return;

            ModAssemblies.Remove(matchingEntry);
            RemoveTypesLoadedBy(matchingEntry);
        }

        private static void RefreshTypesLoadedBy(AssemblyEntry entry)
        {
            RemoveTypesLoadedBy(entry);
            AddTypesLoadedBy(entry);
        }
        private static void AddTypesLoadedBy(AssemblyEntry entry)
        {

            allTypes.AddRange(entry.DefTypes);
        }
        private static void RemoveTypesLoadedBy(AssemblyEntry entry)
        {
            foreach(Type? type in entry.DefTypes)
            {
                allTypes.Remove(type);
            }
        }
    }

    public class AssemblyEntry
    {
        public Assembly Assembly;
        public List<Type> DefTypes;

        public AssemblyEntry(Assembly assembly)
        {
            Update(assembly);
        }

        public virtual void Update(Assembly assembly)
        {
            Assembly = assembly;
            DefTypes = LoadDefTypes().ToList();
        }

        private IEnumerable<Type> LoadDefTypes()
        {
            Type? mainDefType = Assembly?.GetType("Verse.Def");
            if (mainDefType == null)
                yield break;

            foreach (Type type in Assembly.GetTypes())
            {
                if (!type.IsAssignableTo(mainDefType))
                    continue;
                var menuItem = new MenuItem
                {
                    Name = $"MenuItemDefType{type.Name}",
                    Header = type.Name
                };
                menuItem.Click += delegate
                {
                    MainWindow.TextChangeManager.InsertTemplatedDef(type);
                };
                MainWindow.DefTypeMenuItem.Items.Add(menuItem);
                yield return type;
            }
        }
    }

    public class RWAssemblyEntry : AssemblyEntry
    {
        public Type? UnsavedAttribute;
        public Type? DefaultValueAttribute;

        public RWAssemblyEntry(Assembly assembly) : base(assembly) { }

        public override void Update(Assembly assembly)
        {
            base.Update(assembly);
            UnsavedAttribute = assembly.GetType("Verse.UnsavedAttribute");
            DefaultValueAttribute = assembly.GetType("Verse.DefaultValueAttribute");
        }
    }

}
