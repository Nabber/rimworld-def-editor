﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace RimWorld_Def_Editor
{
    internal static class Common
    {
        public static object UserTextChangeTag = "User text change";
        public const string DefaultNewFileName = "New File";
        public const string DefaultNewFileContent = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n<Defs>\n\t\n</Defs>";

        
        /// Unused, this is the > key on my keyboard, as reference to why this approach may not work on different keyboard languages
        //public const Key GreaterThanKey = Key.Oem102;
    }
}
