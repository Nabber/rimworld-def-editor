﻿using ICSharpCode.AvalonEdit;
using PromptDialog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace RimWorld_Def_Editor
{
    public partial class MainWindow
    {
        private void txtTabXMLInput_TextInput(object sender, TextCompositionEventArgs e)
        {
            if (!(sender is TextEditor editor))
                return;
            TextChangeManager.SetActiveEditor(editor);
            char inputChar = e.Text[0];
            e.Handled = TextChangeManager.HandleInputChar(inputChar);
        }

        private void btnTabClose_Click(object sender, RoutedEventArgs e)
        {
            tabManager.CloseTab();
        }

        private void btnTabSave_Click(object sender, RoutedEventArgs e)
        {
            tabManager.CurrentTab.Save();
        }

        private void btnTabSaveAs_Click(object sender, RoutedEventArgs e)
        {
            tabManager.CurrentTab.SaveAs();
        }

        private void btnFilePathPicker(object sender, RoutedEventArgs e)
        {
            tabManager.PickPath();
        }

        private void checkAutoValidate_Checked(object sender, RoutedEventArgs e)
        {
            btnValidationTimer.Content = Validator.TimeToNextValidation;
            checkIsAutoValidationEnabled.IsChecked = true;
            btnValidationTimer.Visibility = Visibility.Visible;
            btnManualValidation.Visibility = Visibility.Collapsed;
            Validator.EnableAutoValidator();
        }

        private void checkAutoValidate_Unchecked(object sender, RoutedEventArgs e)
        {
            checkIsAutoValidationEnabled.IsChecked = false;
            btnValidationTimer.Visibility = Visibility.Collapsed;
            btnManualValidation.Visibility = Visibility.Visible;
            Validator.DisableAutoValidator();
        }
        private void checkPreSaveValidate_Checked(object sender, RoutedEventArgs e)
        {
            checkIsPreSaveValidationEnabled.IsChecked = true;
            Validator.IsPreSaveValidationEnabled = true;
        }

        private void checkPreSaveValidate_Unchecked(object sender, RoutedEventArgs e)
        {
            checkIsPreSaveValidationEnabled.IsChecked = false;
            Validator.IsPreSaveValidationEnabled = false;
        }

        private void btnTabValidate_Click(object sender, RoutedEventArgs e)
        {
            Validator.Validate();
        }

        private void btnValidationTimer_Click(object sender, MouseButtonEventArgs e)
        {
            Dialog dialog = new Dialog("Insert new Validation interval", "Interval", Validator.ValidationInterval.ToString(), Dialog.InputType.Numero);
            dialog.ShowDialog();
            if (!(dialog.ResponseText is string response))
                return;

            if (int.TryParse(response, out int parsedResponse))
            {
                Validator.ValidationInterval = parsedResponse;
                Validator.TimeToNextValidation = parsedResponse;
                btnValidationTimer.Content = Validator.TimeToNextValidation;
            }
        }
    }
}
