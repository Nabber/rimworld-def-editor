﻿using ICSharpCode.AvalonEdit.Document;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;
using System.Xml;

namespace RimWorld_Def_Editor
{
    public class XMLValidator
    {
        private TextDocument? CurrentXMLDocument => TabManager.Instance.CurrentTab?.Document;
        private Button btnAutoValidationTimer;
        private TextBox txtCurrentIssuesInfo;
        private TextBox txtColorizedIssues;
        public static XMLValidator Instance { get; private set; }
        private readonly DispatcherTimer timer;

        public bool IsCurrentlyValid;
        public bool IsPreSaveValidationEnabled;
        public bool IsAutoValidationEnabled;
        
        public int ValidationInterval = 5;
        public int TimeToNextValidation;

        public XMLValidator(Button btnAutoValidationTimer, TextBox txtCurrentIssuesInfo, TextBox txtColorizedIssues)
        {
            Instance = this;
            timer = new DispatcherTimer();
            this.btnAutoValidationTimer = btnAutoValidationTimer;
            this.txtCurrentIssuesInfo = txtCurrentIssuesInfo;
            this.txtColorizedIssues = txtColorizedIssues;
            InitTimer();
        }

        public void Validate()
        {
            if (CurrentXMLDocument == null)
                return;
            try
            {
                XmlReader reader = XmlReader.Create(new StringReader(CurrentXMLDocument.Text));
                while (reader.Read()) { }
            }
            catch (XmlException e)
            {
                SetInvalid(e.Message);
                return;
            }
            SetValid();
        }

        public void SetValid()
        {
            IsCurrentlyValid = true;
            txtCurrentIssuesInfo.Text = string.Empty;
            txtColorizedIssues.Background = new SolidColorBrush(Colors.LightGreen);
        }

        public void SetInvalid(string reason)
        {
            IsCurrentlyValid = false;
            txtCurrentIssuesInfo.Text = reason;
            txtColorizedIssues.Background = new SolidColorBrush(Colors.Red);
        }

        public void EnableAutoValidator()
        {
            timer.Start();
        }

        public void DisableAutoValidator()
        {
            timer.Stop();
            TimeToNextValidation = ValidationInterval;
        }

        void InitTimer()
        {
            timer.Interval = TimeSpan.FromSeconds(1);
            TimeToNextValidation = ValidationInterval;
            timer.Tick += (_, a) =>
            {
                TimeToNextValidation--;
                btnAutoValidationTimer.Content = TimeToNextValidation;
                if (TimeToNextValidation == 0)
                {
                    timer.Stop();
                    TimeToNextValidation = ValidationInterval;
                    Validate();
                    timer.Start();
                }
            };
        }
    }

}
