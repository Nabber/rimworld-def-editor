﻿using Dragablz;
using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Highlighting;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Threading;
using System.Xml;
using MessageBox = System.Windows.MessageBox;

namespace RimWorld_Def_Editor
{
    public class TabManager
    {
        //public CustomXMLValidator AutoValidator { get; private set; }
        public static TabManager Instance { get; private set; }
        readonly TabablzControl tabControl;
        ObservableCollection<XMLTab> tabs;

        public XMLTab CurrentTab => (XMLTab)tabControl.SelectedItem;

        public TabManager(TabablzControl tabControl)
        {
            Instance = this;
            this.tabControl = tabControl;
            tabs = new ObservableCollection<XMLTab>();
            tabControl.DataContext = tabs;
            tabControl.NewItemFactory = CreateBlankTab;
        }

        public void CreateFileTab(TreeViewItem node)
        {
            object fileTag = node.Tag;
            string? filePath = fileTag as string;
            if (filePath == null)
                return;

            XMLTab? tab = tabs.FirstOrDefault(t => t.FilePath == filePath);
            if(tab == null)
            {
                tab = new XMLTab(filePath);
                tabControl.AddToSource(tab);
            }
            
            tabControl.SelectedItem = tab;
        }

        public XMLTab CreateBlankTab()
        {
            var tabItem = new XMLTab();
            return tabItem;
        }
        
        public void CloseTab()
        {
            tabs.Remove(CurrentTab);
        }

        public void PickPath()
        {
            CurrentTab.PickPath();
            if(MessageBox.Show("Load file content?", "Confirm file load", MessageBoxButton.OKCancel) != MessageBoxResult.OK)
                return;
            CurrentTab.LoadFileContent();
        }
    }

    public class XMLTab : DataTemplate, INotifyPropertyChanged
    {
        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler? PropertyChanged;
        public virtual void RaisePropertyChanged(string propertyName)
        {
            OnPropertyChanged(propertyName);
        }
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler? handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }
        #endregion

        string filePath;
        public string FilePath 
        { get => filePath; set
            {
                filePath = value;
                RaisePropertyChanged(nameof(FilePath));
            }
        }
        string fileName;
        public string FileName
        {
            get => fileName; set
            {
                fileName = value;
                RaisePropertyChanged(nameof(FileName));
            }
        }
        public TextDocument Document { get; set; }

        public XMLTab()
        {
            FileName = Common.DefaultNewFileName;
            Document = new TextDocument()
            {
                Text = Common.DefaultNewFileContent
            };
        }

        public XMLTab(string fileName) : this()
        {
            SetNewPath(fileName);
            LoadFileContent();
        }

        public void SetNewPath(string filePath)
        {
            if (!File.Exists(filePath))
                return;
            FilePath = filePath;
            FileName = Path.GetFileName(filePath);
        }

        public void LoadFileContent()
        {
            if (FilePath == null || !File.Exists(FilePath))
                return;
            SetText(File.ReadAllText(FilePath));
        }

        public void SetText(string text)
        {
            Document.Text = text;
            RaisePropertyChanged(nameof(Document));
        }

        public void PickPath()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.DefaultExt = ".xml";
            DialogResult result = dialog.ShowDialog();
            if (result != DialogResult.OK)
                return;
            SetNewPath(dialog.FileName);
        }

        public void SaveAs()
        {
            PickPath();
            Save();
        }
        public void Save()
        {
            if (MainWindow.Validator.IsPreSaveValidationEnabled)
            {
                MainWindow.Validator.Validate();
                if (!MainWindow.Validator.IsCurrentlyValid)
                {
                    MessageBox.Show($"File was not saved - it is currently invalid. Refer to the Current Issues", "XML Error");
                    return;
                }
            }
            if (FilePath == null || !File.Exists(FilePath))
                PickPath();
            if (FilePath == null || !File.Exists(FilePath))
            {
                MessageBox.Show("Could not save file, invalid file path", "Error");
                return;
            }
            File.WriteAllText(FilePath, Document.Text);
        }


    }
}
