﻿using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.CodeCompletion;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Editing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace RimWorld_Def_Editor
{
    public class XMLTextChangeManager
    {
        // how many characters to go back when looking for current input, this will prevent massive documents from starting at index 0 and dragging performance
        private const int caretBackwardsRange = 100;
        private TextEditor? currentEditor;
        private Type activeDefType;
        public readonly ComboBox defTypePresenter;
        List<CompletionData> completionData = new List<CompletionData>();

        public XMLTextChangeManager(ComboBox defTypePresenter)
        {
            this.defTypePresenter = defTypePresenter;
            this.defTypePresenter.ItemsSource = AssemblyManager.AllTypes;
        }

        public void SetActiveEditor(TextEditor? editor)
        {
            currentEditor = editor;
        }

        private string? CurrentLine
        {
            get
            {
                if (currentEditor == null)
                    return null;
                DocumentLine line = currentEditor.Document.GetLineByOffset(currentEditor.CaretOffset);
                return currentEditor.Document.GetText(line.Offset, line.Length);
            }
        }

        /// <summary>
        /// Not sure if this is a brilliant idea, but the best I can come up with rn
        /// </summary>
        List<char> IndentationCharacters = new List<char>()
        {
            ' ',
            '\t'
        };

        private string? CurrentIndentationString
        {
            get
            {
                string? line = CurrentLine;
                if (line == null)
                    return null;
                return new String(line.TakeWhile(c => IndentationCharacters.Contains(c)).ToArray());
            }
        }

        // TODO maybe refactor use caret offset directly and step through all charAt(i) with negative loop
        private string? CurrentXMLTag => CurrentLine == null ? "" : new string(CurrentLine
                .Reverse()
                .SkipWhile(c => c != '>')
                .Skip(1)
                .TakeWhile(c => c != '<')
                .Reverse()
                .ToArray());

        /// <returns>TRUE if the key was handled by the code instead</returns>
        public bool HandleInputChar(char input)
        {
            if (currentEditor == null)
                return false;
            int offset = currentEditor.CaretOffset;
            currentEditor.Document.Text = currentEditor.Document.Text.Insert(offset, input.ToString());
            currentEditor.CaretOffset = offset + 1;
            switch (input)
            {
                case '<':
                    DoTagRecommendations();
                    break;
                case '>':
                    CheckForActiveType();
                    DoTagAutoClose();
                    break;
            }
            return true;
        }

        public void DoTagRecommendations()
        {
            if (currentEditor == null)
                return;
            if (activeDefType == null)
                return;
            if (!completionData.Any())
                return;
            CompletionWindow window = new CompletionWindow(currentEditor.TextArea);
            foreach (CompletionData data in completionData)
                window.CompletionList.CompletionData.Add(data);
            window.Show();
        }

        private void CheckForActiveType()
        {
            string? currentTag = CurrentXMLTag;
            if (currentTag == null)
                return;
            Type? newType = AssemblyManager.AllTypes.FirstOrDefault(t =>
                t.Name == currentTag 
                || t.FullName == currentTag
            );
            if (newType == null)
                return;
            SetActiveDefType(newType);
        }

        public void SetActiveDefType(Type type)
        {
            if(defTypePresenter.SelectedItem as Type != type)
            {
                // the SelectionChanged event will re-call this method and properly set completion data
                defTypePresenter.SelectedItem = type;
                return;
            }
            activeDefType = type;
            completionData.Clear();
            completionData = GenerateCompletionData(type);
        }

        public static Dictionary<Type, List<CompletionData>> cachedCompletionData = new Dictionary<Type, List<CompletionData>>();
        private static List<CompletionData> GenerateCompletionData(Type type)
        {
            if (AssemblyManager.RWAssembly == null)
                return new List<CompletionData>();

            if (!cachedCompletionData.ContainsKey(type))
            {
                List<CompletionData>? typeCompletionData = type.GetFields()
                    .Where(field => !field.IsUnsavedField())
                    .Select(field => new CompletionData($"{field.Name}>", $"</{field.Name}>", field.Name))
                    .ToList();
                cachedCompletionData.Add(type, typeCompletionData);
            }

            return cachedCompletionData[type];
        }

        private void DoTagAutoClose()
        {
            if (currentEditor == null)
                return;
            int offset = currentEditor.CaretOffset;
            if (offset < 1)
                return;

            string? currentXMLTag = CurrentXMLTag;
            if (currentXMLTag == null || !IsXMLTagValid(currentXMLTag))
                return;

            currentEditor.Document.Text = currentEditor.Document.Text.Insert(offset, $"</{currentXMLTag}>");
            currentEditor.CaretOffset = offset;
        }

        public void InsertTemplatedDef(Type type)
        {
            if (currentEditor == null)
            {
                MessageBox.Show("Create a XML tab first", "Info");
                return;
            }
            if (AssemblyManager.RWAssembly == null)
            {
                MessageBox.Show("No RimWorld assembly loaded.", "Info");
                return;
            }

            InsertWithRespectedIndentation(CreateFullDefNode(type));

            //currentEditor.Text = currentEditor.Text.Insert(caretOffset, CreateFullDefNode(type));

            //currentEditor.CaretOffset = caretOffset;
        }

        private void InsertWithRespectedIndentation(string insertionText)
        {
            int offset = currentEditor.CaretOffset;
            string currentIndentationString = CurrentIndentationString ?? String.Empty;
            string[] lines = insertionText.Split('\n');
            for (int i = 0; i < lines.Length; i++)
            {
                string line = lines[i];
                // first line is already indented, last line needs no linebreak
                string insertLine;
                if(i == 0)
                    insertLine = $"{line}\n";
                else if(i == lines.Length - 1)
                    insertLine = $"{currentIndentationString}{line}";
                else
                    insertLine = $"{currentIndentationString}{line}\n";
                currentEditor.Text = currentEditor.Text.Insert(offset, insertLine);
                offset += insertLine.Length;
            }
        }

        private string CreateFullDefNode(Type type)
        {
            Dictionary<string, string> fieldValuePairs = new Dictionary<string, string>();
            var instance = Activator.CreateInstance(type);
            foreach (FieldInfo field in type.GetFields())
            {
                string defaultValue = GetDefaultValueForField(field, instance);
                fieldValuePairs.Add(field.Name, defaultValue);
            }
            string defBody = string.Join("\n", fieldValuePairs
                .OrderBy(kvp => kvp.Key)
                .Select(kvp => $"\t<{kvp.Key}>{kvp.Value}</{kvp.Key}>"));
            return $"<{type.Name}>\n{defBody}\n</{type.Name}>";
        }

        private string GetDefaultValueForField(FieldInfo field, object? instance)
        {
            if (TryRetrieveFromDefaultAttribute(field, out string value))
                return value;
            if (instance != null && TryRetrieveFromInstance(field, instance, out value))
                return value;
            return string.Empty;
        }

        private bool TryRetrieveFromDefaultAttribute(FieldInfo field, out string result)
        {
            result = string.Empty;
            var defaultValue = field.GetCustomAttribute(AssemblyManager.RWAssembly.DefaultValueAttribute);
            if (defaultValue == null)
                return false;
            object? valueObject = AssemblyManager.RWAssembly.DefaultValueAttribute.GetField("value")?.GetValue(defaultValue);
            if (valueObject == null)
                return false;
            result = valueObject.ToString() ?? string.Empty;
            return true;
        }
        private bool TryRetrieveFromInstance(FieldInfo field, object? instance, out string result)
        {
            result = string.Empty;
            object? value = field.GetValue(instance);
            if (value == null)
                return false;
            result = value.ToString() ?? string.Empty;
            return true;
        }

        private readonly static List<char> invalidXMLTagCharacters = new List<char>()
        {
            ' ',
            '/',
            '\\',
            '>',
            '\n',
            '\t'
        };
        private static bool IsXMLTagValid(string tag)
        {
            if (tag.Length == 0)
                return false;
            if (invalidXMLTagCharacters.Any(c => tag.Contains(c)))
                return false;
            return true;
        }
    }

    
    public class CompletionData : ICompletionData
    {
        /// <summary>
        /// auto-insert will move the caret to the end of the inserted text. the caret delta will step back to where the rightText begins, thus placing the user in the middle
        /// </summary>
        private int caretDelta;

        public CompletionData(string text, string content)
        {
            caretDelta = 0;
            Text = text;
            Content = content;
        }

        public CompletionData(string leftText, string rightText, string content)
        {
            Text = leftText + rightText;
            Content = content;
            caretDelta = rightText.Length;
        }

        public ImageSource Image { get; set; }

        /// <summary>
        /// Bad naming in ICompletionData. Text is the replacement-text, it will be inserted
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Bad naming in ICompletionData. Content is the presentation text in the window
        /// </summary>
        public object Content { get; set; }

        public object Description { get; set; }

        public double Priority { get; set; }

        public void Complete(TextArea textArea, ISegment completionSegment, EventArgs insertionRequestEventArgs)
        {
            textArea.Document.Replace(completionSegment, Text);
            textArea.Caret.Offset -= caretDelta;
        }
    }
}
